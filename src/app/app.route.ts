import {Routes} from '@angular/router';
import {RecipeListComponent} from './recipe-list/recipe-list.component';
import {IngredientListComponent} from './ingredient-list/ingredient-list.component';
import {HomeComponent} from './home/home.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'recipe-list',
    component: RecipeListComponent
  },
  {
    path: 'ingredient-list',
    component: IngredientListComponent
  },
];
