import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AppRoutes} from './app.route';

import {AppComponent} from './app.component';
import {IngredientListComponent} from './ingredient-list/ingredient-list.component';
import {RecipeListComponent} from './recipe-list/recipe-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSnackBarModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {AddDialogComponent} from './recipe-list/add-dialog/add-dialog.component';
import {EditDialogComponent} from './recipe-list/edit-dialog/edit-dialog.component';
import {DeleteDialogComponent} from './recipe-list/delete-dialog/delete-dialog.component';
import {FormsModule} from '@angular/forms';
import {AddIngredientDialogComponent} from './ingredient-list/add-ingredient-dialog/add-ingredient-dialog.component';
import {DeleteIngredientDialogComponent} from './ingredient-list/delete-ingredient-dialog/delete-ingredient-dialog.component';
import {EditIngredientDialogComponent} from './ingredient-list/edit-ingredient-dialog/edit-ingredient-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    IngredientListComponent,
    RecipeListComponent,
    HomeComponent,
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    AddIngredientDialogComponent,
    DeleteIngredientDialogComponent,
    EditIngredientDialogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    HttpClientModule,
    MatCardModule,
    MatPaginatorModule,
    MatGridListModule,
    MatFormFieldModule,
    FormsModule,
    MatDialogModule,
    MatSnackBarModule,
    MatInputModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    AddIngredientDialogComponent,
    DeleteIngredientDialogComponent,
    EditIngredientDialogComponent
  ]
})
export class AppModule {
}
