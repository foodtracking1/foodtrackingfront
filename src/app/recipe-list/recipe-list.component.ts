import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IRecipe, Recipe} from '../../model/recipe';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {AddDialogComponent} from './add-dialog/add-dialog.component';
import {EditDialogComponent} from './edit-dialog/edit-dialog.component';
import {DeleteDialogComponent} from './delete-dialog/delete-dialog.component';
import {RecipeService} from '../../service/recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
  providers: [RecipeService]
})
export class RecipeListComponent implements OnInit {
  recipesTableDataSource: MatTableDataSource<IRecipe>;
  displayedColumns: string[] = ['title', 'description', 'actions'];
  id: string;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter', {static: true}) filter: ElementRef;

  constructor(private http: HttpClient,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private recipeService: RecipeService) {
  }

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  loadData() {
    this.recipeService.getAllRecipes().subscribe(response => {
        this.recipesTableDataSource = new MatTableDataSource<IRecipe>(response.content);
        this.recipesTableDataSource.paginator = this.paginator;
        this.recipesTableDataSource._filterData(response.content);
      },
      error => {
        this.snackBar.open(error.name + ' ' + error.message, 'Oh..');
      });
  }

  addNew() {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: {recipe: new Recipe()},
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  startEdit(id: string, title: string, description: string) {
    this.id = id;
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: {id, title, description},
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  deleteRecipe(id: string, title: string, description: string) {
    this.id = id;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {id, title, description},
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }
}
