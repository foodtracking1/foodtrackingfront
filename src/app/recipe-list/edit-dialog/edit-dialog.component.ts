import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatPaginator, MatSnackBar, MatTableDataSource} from '@angular/material';
import {RecipeService} from '../../../service/recipe.service';
import {FormControl, Validators} from '@angular/forms';
import {IIngredient} from '../../../model/ingredient';
import {SelectionModel} from '@angular/cdk/collections';
import {IngredientService} from '../../../service/ingredient.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css'],
  providers: [RecipeService, IngredientService]
})
export class EditDialogComponent implements OnInit {
  ingredientsTableDataSource: MatTableDataSource<IIngredient>;
  displayedColumns: string[] = ['select', 'name'];
  selection = new SelectionModel<IIngredient>(true, []);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public recipeService: RecipeService,
              public ingredientService: IngredientService,
              private snackBar: MatSnackBar) {
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);


  ngOnInit() {
    this.loadIngredients();
  }

  loadIngredients() {
    this.ingredientService.getAllIngredients().subscribe(response => {
      this.ingredientsTableDataSource = new MatTableDataSource<IIngredient>(response.content);
      this.ingredientsTableDataSource.paginator = this.paginator;

      this.recipeService.getIngredientsByRecipeId(this.data.id).subscribe(value => {
          value.content.forEach(value1 => {
            this.selection.select(value1);
          });
        },
        error => {
          this.snackBar.open(error.name + ' ' + error.message, 'Oh..');
        });
    }, error => {
      this.snackBar.open(error.name + ' ' + error.message, 'Oh..');
    });

  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    console.log(this.data);
    this.data.ingredients = this.selection.selected;
    this.recipeService.updateRecipe(this.data).subscribe(() => {
        this.snackBar.open('Updated successfully', 'Okay !');
      },
      error => {
        this.snackBar.open(error.name + ' ' + error.message, 'Oh..');
      });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.ingredientsTableDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.ingredientsTableDataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: IIngredient): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }
}
