import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {RecipeService} from '../../../service/recipe.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css'],
  providers: [RecipeService]
})
export class DeleteDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dataService: RecipeService,
              private snackBar: MatSnackBar) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteRecipe(this.data.id).subscribe(() => {
        this.snackBar.open('Deleted successfully', 'Okay !');
      },
      error => {
        this.snackBar.open(error.name + ' ' + error.message, 'Oh..');
      });
  }

  ngOnInit() {
  }

}
