import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import {IRecipe} from '../../../model/recipe';
import {RecipeService} from '../../../service/recipe.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.css'],
  providers: [RecipeService]
})
export class AddDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IRecipe,
              public dataService: RecipeService,
              public snackBar: MatSnackBar) {
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  ngOnInit() {
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addRecipe(this.data).subscribe(() => {
        this.snackBar.open('Added successfully', 'Nice !');
      },
      error => {
        this.snackBar.open(error.name + ' ' + error.message, 'Oh..');
      });
  }

}
