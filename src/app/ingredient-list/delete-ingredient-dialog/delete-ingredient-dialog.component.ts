import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IngredientService} from '../../../service/ingredient.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-ingredient-dialog.component.html',
  styleUrls: ['./delete-ingredient-dialog.component.css'],
  providers: [IngredientService]
})
export class DeleteIngredientDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteIngredientDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: IngredientService) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteIngredient(this.data.id);
  }

  ngOnInit() {
  }

}
