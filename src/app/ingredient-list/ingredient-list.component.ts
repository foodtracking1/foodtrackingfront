import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ArrayPage} from '@hallysonh/pageable';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {IIngredient, Ingredient} from '../../model/ingredient';
import {AddIngredientDialogComponent} from './add-ingredient-dialog/add-ingredient-dialog.component';
import {EditIngredientDialogComponent} from './edit-ingredient-dialog/edit-ingredient-dialog.component';
import {DeleteIngredientDialogComponent} from './delete-ingredient-dialog/delete-ingredient-dialog.component';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css']
})
export class IngredientListComponent implements OnInit {

  private readonly API_URL = 'http://localhost:8080/ingredients/';

  ingredientArrayPage: ArrayPage<IIngredient>;
  ingredientsTableDataSource: MatTableDataSource<IIngredient>;
  displayedColumns: string[] = ['name', 'actions'];
  id: string;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter', {static: true}) filter: ElementRef;

  constructor(private http: HttpClient,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
    this.ingredientsTableDataSource._filterData(this.ingredientArrayPage.content);
  }

  loadData() {
    this.http.get<ArrayPage<IIngredient>>(this.API_URL).subscribe(data => {
        this.ingredientArrayPage = data;
        this.ingredientsTableDataSource = new MatTableDataSource<IIngredient>(data.content);
        this.ingredientsTableDataSource.paginator = this.paginator;
      },
      (error: HttpErrorResponse) => {
        this.snackBar.open(error.name + ' ' + error.message, 'Oh..');
      });

  }

  addNew() {
    const dialogRef = this.dialog.open(AddIngredientDialogComponent, {
      data: {ingredient: Ingredient},
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  startEdit(id: string, name: string) {
    this.id = id;
    const dialogRef = this.dialog.open(EditIngredientDialogComponent, {
      data: {id, name},
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  deleteRecipe(id: string, name: string) {
    this.id = id;
    const dialogRef = this.dialog.open(DeleteIngredientDialogComponent, {
      data: {id, name},
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

}
