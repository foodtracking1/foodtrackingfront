import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import {IIngredient} from '../../../model/ingredient';
import {IngredientService} from '../../../service/ingredient.service';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-ingredient-dialog.component.html',
  styleUrls: ['./add-ingredient-dialog.component.css'],
  providers: [IngredientService]
})
export class AddIngredientDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddIngredientDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IIngredient,
              public dataService: IngredientService) {
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  ngOnInit() {
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addIngredient(this.data);
  }

}
