import {HttpClient} from '@angular/common/http';
import {IRecipe} from '../model/recipe';
import {MatSnackBar} from '@angular/material';
import {ArrayPage} from '@hallysonh/pageable';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IIngredient} from '../model/ingredient';

@Injectable()
export class RecipeService {
  private readonly API_URL = 'http://localhost:8080/recipes/';

  constructor(private httpClient: HttpClient) {
  }

  getAllRecipes(): Observable<ArrayPage<IRecipe>> {
    return this.httpClient.get<ArrayPage<IRecipe>>(this.API_URL);
  }

  addRecipe(recipe: IRecipe): Observable<IRecipe> {
    return this.httpClient.post<IRecipe>(this.API_URL, recipe);
  }

  updateRecipe(recipe: IRecipe): Observable<IRecipe> {
    return this.httpClient.put<IRecipe>(this.API_URL + recipe.id, recipe);
  }

  deleteRecipe(id: string): Observable<any> {
    return this.httpClient.delete(this.API_URL + id);
  }

  getIngredientsByRecipeId(id: string): Observable<ArrayPage<IIngredient>> {
    return this.httpClient.get<ArrayPage<IIngredient>>(this.API_URL + id + '/ingredients');
  }

  addIngredientByRecipeId(recipeId: string, ingredient: IIngredient): Observable<IRecipe> {
    return this.httpClient.post<IRecipe>(this.API_URL + recipeId + '/ingredients', ingredient);
  }

  deleteIngredientByRecipeId(recipeId: string, ingredientId: string): Observable<any> {
    return this.httpClient.delete(this.API_URL + recipeId + '/ingredients/' + ingredientId);
  }
}
