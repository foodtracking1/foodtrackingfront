import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {ArrayPage} from '@hallysonh/pageable';
import {Injectable} from '@angular/core';
import {IIngredient} from '../model/ingredient';
import {Observable} from 'rxjs';

@Injectable()
export class IngredientService {
  private readonly API_URL = 'http://localhost:8080/ingredients/';

  constructor(private httpClient: HttpClient, private snackBar: MatSnackBar) {
  }

  /** CRUD METHODS */
  getAllIngredients(): Observable<ArrayPage<IIngredient>> {
    return this.httpClient.get<ArrayPage<IIngredient>>(this.API_URL);
  }

  // ADD, POST METHOD
  addIngredient(ingredient: IIngredient): void {
    this.httpClient.post(this.API_URL, ingredient).subscribe(() => {
        this.snackBar.open('Successfully added', 'Okay !');
      },
      (err: HttpErrorResponse) => {
        this.snackBar.open('Error occurred. Details: ' + err.name + ' ' + err.message, 'Oh, Okay...');
      });
  }

  // UPDATE, PUT METHOD
  updateIngredient(ingredient: IIngredient): void {
    this.httpClient.put(this.API_URL + ingredient.id, ingredient).subscribe(() => {
        this.snackBar.open('Successfully edited', 'Nice !');
      },
      (err: HttpErrorResponse) => {
        this.snackBar.open('Error occurred. Details: ' + err.name + ' ' + err.message, 'Oh damn !');
      }
    );
  }

  // DELETE METHOD
  deleteIngredient(id: number): void {
    this.httpClient.delete(this.API_URL + id).subscribe(() => {
        this.snackBar.open('Successfully deleted', 'Bye, ingredient !');
      },
      (err: HttpErrorResponse) => {
        this.snackBar.open('Error occurred. Details: ' + err.name + ' ' + err.message, 'Oh crap !');
      }
    );
  }
}
