export interface IIngredient {
  id: string;
  name: string;
}

export class Ingredient implements IIngredient {
  id: string;
  name: string;

}
