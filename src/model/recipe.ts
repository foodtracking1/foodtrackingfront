import {IIngredient} from './ingredient';

export interface IRecipe {
  id: string;
  title: string;
  description: string;
  ingredients: IIngredient[];
}

export class Recipe implements IRecipe {
  description: string;
  id: string;
  title: string;
  ingredients: IIngredient[];
}
